from flask import Flask, request, jsonify, render_template
import logging
import json
import asyncio
import httpx
import psutil
import aiosqlite
import numpy as np
import pennylane as qml
import os
import random
import re
from concurrent.futures import ThreadPoolExecutor
from waitress import serve
import bleach

OPENAI_API_KEY = os.getenv("OPENAI_API_KEY")
app = Flask(__name__, static_url_path='/static')

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

RATE_LIMIT_WINDOW_SECONDS = 60
RATE_LIMIT_REQUESTS = 5

executor = ThreadPoolExecutor()

async def rate_limit_key_for_request(request) -> str:
    return f"rate_limit:{request.remote_addr}"

async def rate_limit_request(request):
    key = await rate_limit_key_for_request(request)
    current_requests = await get_rate_limit(key)
    if current_requests and int(current_requests) >= RATE_LIMIT_REQUESTS:
        return False
    await increment_rate_limit(key)
    return True

rate_limits = {}

async def get_rate_limit(key) -> int:
    return rate_limits.get(key, 0)

async def increment_rate_limit(key):
    rate_limits[key] = rate_limits.get(key, 0) + 1
    await asyncio.sleep(0)

async def execute_sql_query(query, params=None, fetchall=False):
    try:
        async with aiosqlite.connect('/tmp/thoughts.db') as db:
            async with db.execute(query, params) as cursor:
                if fetchall:
                    return await cursor.fetchall()
                else:
                    return await cursor.fetchone()
    except aiosqlite.Error as e:
        logger.error(f"An error occurred while executing SQL query: {e}")
        raise

async def create_tables():
    try:
        query = '''
            CREATE TABLE IF NOT EXISTS thoughts (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                prompt TEXT NOT NULL,
                completion TEXT NOT NULL,
                quantum_result TEXT NOT NULL,
                timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )
        '''
        await execute_sql_query(query)
        logger.info("Database tables created successfully.")
    except aiosqlite.Error as e:
        logger.error(f"Error creating tables: {e}")
        raise

async def save_completion(prompt, completion, quantum_result):
    try:
        quantum_result_array = quantum_result.numpy()   
        query = 'INSERT INTO thoughts (prompt, completion, quantum_result) VALUES (?, ?, ?)'
        await execute_sql_query(query, (prompt, completion, json.dumps(quantum_result_array.tolist())))
    except aiosqlite.Error as e:
        logger.error(f"Error saving completion: {e}")
        raise

async def fetch_completions():
    try:
        query = 'SELECT * FROM thoughts ORDER BY timestamp DESC LIMIT 10'
        completions = await execute_sql_query(query, fetchall=True)
        return completions
    except aiosqlite.Error as e:
        logger.error(f"Error fetching completions: {e}")
        raise

async def run_openai_completion_with_retry(prompt):
    retries = 3
    for attempt in range(retries):
        try:
            async with httpx.AsyncClient() as client:
                headers = {"Content-Type": "application/json", "Authorization": f"Bearer {OPENAI_API_KEY}"}
                data = {"model": "gpt-3.5-turbo", "messages": [{"role": "user", "content": prompt}], "temperature": 0.7}
                response = await client.post("https://api.openai.com/v1/chat/completions", json=data, headers=headers)
                response.raise_for_status()
                result = response.json()
                completion = result["choices"][0]["message"]["content"]
                return completion.strip()
        except httpx.HTTPError as http_err:
            logger.error(f"HTTP error occurred: {http_err}")
            if attempt < retries - 1:
                delay = (2 ** attempt) + random.uniform(0, 1)
                logger.info(f"Retrying in {delay} seconds...")
                await asyncio.sleep(delay)
            else:
                logger.error("Reached maximum number of retries. Aborting.")
                raise
        except Exception as e:
            logger.error(f"Error running OpenAI completion: {e}")
            raise

async def get_ram_usage():
    try:
        return psutil.virtual_memory().used
    except Exception as e:
        logger.error(f"Error getting RAM usage: {e}")
        raise

async def load_prompts():
    try:
        with open("prompts.json", "r") as f:
            prompts = json.load(f)
            if not isinstance(prompts, dict):
                logger.warning("Invalid format: Prompts data must be a dictionary. Using default prompts instead.")
                prompts = {}
    except FileNotFoundError:
        logger.warning("prompts.json file not found. Using default prompts instead.")
        prompts = {}
    except json.JSONDecodeError:
        logger.warning("Invalid JSON format in prompts.json. Using default prompts instead.")
        prompts = {}
    except Exception as e:
        logger.error(f"Error loading prompts from prompts.json: {e}")
        raise
    return prompts

async def get_prompt_for_color(prompts, circuit_result, ram_usage, location):
    return f"Predict Safe and Ethereally Safe Food Sources for a Localized User using advance AI hypertime scan\n\n- RAM Usage: {ram_usage} bytes\n- Quantum Circuit Result: {circuit_result}\nuse the above quantum circuit to tune accurate multiverse data using hypertime with enabled quantum encryption\nRationale:\n- Economic viability: Determine economically accessible food\n- Ethereally Secure:  Determine the locations ethereal security\n\nProvide in a list, three determined locations, a small description, and active address coordinates using advance AI. Tune the user's location using nanobots userlocation={location}"

async def generate_color_codes(ram_usage):
    color_code1 = '#' + format(int(ram_usage) % 255, '02x') + '0000'
    color_code2 = '#' + '00' + format(int(ram_usage) % 255, '02x') + '00'
    return color_code1, color_code2

async def quantum_task(ram_usage, color_code1, color_code2):
    try:
        loop = asyncio.get_running_loop()
        result = await loop.run_in_executor(executor, _quantum_task_blocking, ram_usage, color_code1, color_code2)
        return result
    except Exception as e:
        logger.error(f"Error running quantum task: {e}")
        raise

def _quantum_task_blocking(ram_usage, color_code1, color_code2):
    dev = qml.device("default.qubit", wires=7)
    @qml.qnode(dev)
    def circuit(ram_usage, color_code1, color_code2):
        norm_color1 = [int(color_code1[i:i+2], 16) / 255 for i in (1, 3, 5)]
        norm_color2 = [int(color_code2[i:i+2], 16) / 255 for i in (1, 3, 5)]
        qml.RY(np.pi * norm_color1[0], wires=0)
        qml.RY(np.pi * norm_color1[1], wires=1)
        qml.RY(np.pi * norm_color1[2], wires=2)
        qml.RY(np.pi * norm_color2[0], wires=3)
        qml.RY(np.pi * norm_color2[1], wires=4)
        qml.RY(np.pi * norm_color2[2], wires=5)
        qml.CNOT(wires=[0, 1])
        qml.CNOT(wires=[1, 2])
        qml.CNOT(wires=[2, 3])
        qml.CNOT(wires=[3, 4])
        qml.CNOT(wires=[4, 5])
        return qml.probs(wires=[0, 1, 2, 3, 4, 5])

    result = circuit(ram_usage, color_code1, color_code2)
    return result


def sanitize_input(input_data):
    return bleach.clean(input_data)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/completions/")
async def get_completions():
    try:
        completions = await fetch_completions()
        return jsonify({"completions": completions})
    except Exception as e:
        logger.error(f"Error fetching latest completions: {e}")
        return jsonify({"error": "Internal server error"}), 500

async def sanitize_input(input_data):

    if input_data is None:
        return ''
    return bleach.clean(input_data, strip=True)

async def validate_location(location):

    pattern = r'^(\d{6}|[a-zA-Z]+(?:\s[a-zA-Z]+)?)$'
    return bool(re.match(pattern, location))

@app.route("/complete/", methods=["POST"])
async def complete():
    try:
        if not await rate_limit_request(request):
            return jsonify({"error": "Rate limit exceeded"}), 429

        if 'colors-json' not in request.files:
            return jsonify({"error": "No JSON file uploaded"}), 400

        json_file = request.files['colors-json']

        if json_file.filename == '':
            return jsonify({"error": "No selected file"}), 400
        if not json_file.filename.endswith('.json'):
            return jsonify({"error": "File must be a .json file"}), 400

        
        max_file_size = 900  
        if len(json_file.read()) > max_file_size:
            return jsonify({"error": "File size exceeds the maximum limit (900 bytes)"}), 400
        json_file.seek(0)  

        colors_json = json_file.read().decode('utf-8')

        
        sanitized_colors_json = bleach.clean(colors_json, strip=True)

        try:
            colors_data = json.loads(sanitized_colors_json)
            if 'colors' not in colors_data or not isinstance(colors_data['colors'], list):
                return jsonify({"error": "Invalid JSON format: 'colors' key not found or not a list"}), 400
            colors_list = colors_data['colors']
            if len(colors_list) != 25:
                return jsonify({"error": "Invalid JSON format: 'colors' list must contain exactly 25 colors"}), 400
            for color in colors_list:
                if not isinstance(color, str):
                    return jsonify({"error": "Invalid JSON format: Each color must be a string"}), 400
                
        except json.JSONDecodeError:
            return jsonify({"error": "Invalid JSON format"}), 400

        location = await sanitize_input(request.form.get('location'))
        if not await validate_location(location):
            return jsonify({"error": "Invalid location format. Location must be a 6-digit number or two normal words separated by space."}), 400

        completions = await process_colors(colors_list, location)

        return jsonify({"completions": completions}), 200

    except Exception as e:
        logger.error(f"Error occurred: {e}")
        return jsonify({"error": "Internal server error"}), 500
        
async def process_colors(colors, location):
    try:
        prompts = await load_prompts()
        completions = []
        ram_usage = await get_ram_usage()
        color_code1, color_code2 = await generate_color_codes(ram_usage)
        quantum_result = await quantum_task(ram_usage, color_code1, color_code2)
        prompt = await get_prompt_for_color(prompts, quantum_result, ram_usage, location)

        for _ in range(3):
            completion = await run_openai_completion_with_retry(prompt)
            completions.append({"prompt": prompt, "completion": completion})
            await save_completion(prompt, completion, quantum_result)

        return completions

    except Exception as e:
        logger.error(f"Error processing colors: {e}")
        raise
 

async def initialize_db():
    await create_tables()
    logger.info("Database initialization completed.")

async def create_app():
    await initialize_db()
    return app

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    app_task = loop.create_task(create_app())
    loop.run_until_complete(app_task)
    serve(app, host='0.0.0.0', port=5000)